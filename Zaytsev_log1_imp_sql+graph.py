import pandas as pd
import numpy as np
from datetime import datetime, date, time
import collections
import time
import re
from datetime import timedelta
import matplotlib.pyplot as plt
global df

df = pd.read_csv('request.log', names=["Date", "Server ID", "Response generation time",
                                       "System response generation time", "Client IP", "Session and user ID","HTTP method",
                                       "URL", "Refferer URL"], sep ='\t', nrows = 10000) 

ip_list = sorted(df['Client IP'].value_counts().keys().tolist())            #подсчёт количества одинвковых наименований      

time_count =  time.time()

all_request_count = []
session_request_count = []
all_execution_time = []
session_execution_time = []
session_system_execution_time = []
all_system_execution_time = []
   
def count_and_time(data_type,list_name):
    global df   
    for i in range (len(list_name)):
        
        af = df[df[data_type]==list_name[i]]
        if len(af)>137: 
            df = df[df[data_type]!=list_name[i]]
        
        all_request_count.append(len(af))
        session_request_count.append(len(af[(af['Session and user ID'] != '-')]))
        all_execution_time.append(af['Response generation time'].sum())
        session_execution_time.append(af[(af['Session and user ID'] != '-')]['Response generation time'].sum())
        all_system_execution_time.append(af['System response generation time'].sum())
        session_system_execution_time.append(af[(af['Session and user ID'] != '-')]['System response generation time'].sum())
              
count_and_time('Client IP', ip_list)    
sf = pd.DataFrame({
    'ip' : ip_list,
    'all_request_count' : all_request_count,
    'session_request_count' : session_request_count,
    'all_execution_time' : all_execution_time,
    'session_execution_time' : session_execution_time,
    'all_system_execution_time' : all_system_execution_time,
    'session_system_execution_time' : session_system_execution_time,
    
})

sf.to_csv(r'C:\Users\KDFX Team\First.csv')
#sf = sf.cumsum()

plt.figure()   #; sf.plot(x='ip', style='k--', label='Series', x_compat=True,  figsize=(50, 10)); plt.legend()

sf.plot(y=['all_request_count','session_request_count'], x_compat=True)
ax2 = sf.plot(x='ip',secondary_y=[0, 'all_execution_time', 'session_execution_time','all_system_execution_time','session_system_execution_time'],kind='bar', stacked=True, style='--', figsize=(50, 10), ylim =(0)); ax2.grid(); ax2.set_ylim(0)

ax2.set_ylabel('Request count')
ax2.right_ax.set_ylabel('Time(s)')
ax2.get_figure().savefig('output.png')
#sf.plot().get_figure().savefig('output.png')

print('finish')
print(time.time() - time_count)
#=============================================================
df = pd.read_csv('request.log', names=["Date", "Server ID", "Response generation time",
                                       "System response generation time", "Client IP", "Session and user ID",
                                       "HTTP method", "URL", "Refferer URL"], sep ='\t', nrows = 10000) 

df['Date_D'] = pd.to_datetime(df['Date'].values.astype('datetime64[D]'))
date_list = sorted(df['Date_D'].value_counts().keys().tolist())

time_count =  time.time()
     
all_request_count = []
session_request_count = []
all_execution_time = []
session_execution_time = []
session_system_execution_time = []
all_system_execution_time = []
        
count_and_time('Date_D', date_list)    
 
ff = pd.DataFrame({
    'date' : date_list,
    'all_request_count' : all_request_count,
    'session_request_count' : session_request_count,
    'all_execution_time' : all_execution_time,
    'session_execution_time' : session_execution_time,
    'all_system_execution_time' : all_system_execution_time,
    'session_system_execution_time' : session_system_execution_time,
    
})
ff.to_csv(r'C:\Users\KDFX Team\Second.csv')

plt.figure()   #; sf.plot(x='ip', style='k--', label='Series', x_compat=True,  figsize=(50, 10)); plt.legend()

ff.plot(y=['all_request_count','session_request_count'], x_compat=True)
ax2 = ff.plot(x='date',secondary_y=[0, 'all_execution_time', 'session_execution_time','all_system_execution_time','session_system_execution_time'],kind='bar', style='--', figsize=(100, 10), ylim =(0)); ax2.grid(); ax2.set_ylim(0)

'''sf.plot(y=['all_request_count','session_request_count'], x_compat=True)
ax2 = sf.plot(x='ip',secondary_y=[0, 'all_execution_time', 'session_execution_time','all_system_execution_time','session_system_execution_time'],kind='bar', stacked=True, style='--', figsize=(50, 10), ylim =(0)); ax2.grid(); ax2.set_ylim(0)
'''


ax2.set_ylabel('Request count')
ax2.right_ax.set_ylabel('Time(h)')
ax2.get_figure().savefig('output2.png')

print('finish')
print(time.time() - time_count)
#=================================================================
df = pd.read_csv('request.log', names=["Date", "Server ID", "Response generation time",
                                       "System response generation time", "Client IP", "Session and user ID",
                                       "HTTP method", "URL", "Refferer URL"], sep ='\t', nrows = 1000) 

df['Date_T'] = pd.to_datetime(df['Date'].values.astype('datetime64'))
session_id_list = sorted(df.loc[(df['Session and user ID'] != '-')]['Session and user ID'].value_counts().keys().tolist())

time_count =  time.time()

session_id = []
user_id = []
for i in range(len(session_id_list)):
    session_id.append(session_id_list[i].split(':')[1])
    user_id.append(session_id_list[i].split(':')[0])
    
session_request_count  = []
session_execution_time = []
session_system_execution_time = []

df = df[df['Session and user ID']!='-']

for i in range (len(session_id_list)):
    af = df[df['Session and user ID']==session_id_list[i]]
                    
    session_request_count.append(len(af))
    session_execution_time.append(af['Response generation time'].sum())
    session_system_execution_time.append(af['System response generation time'].sum())
 
start_time_index = (df.loc[(df['Session and user ID'].duplicated(keep='first') == False) & (df['Session and user ID'] != '-')]['Date_T'])
end_time_index = (df.loc[(df['Session and user ID'].duplicated(keep='last') == False) & (df['Session and user ID'] != '-')]['Date_T'])

end_time = end_time_index.reset_index(drop = True)
start_time = start_time_index.reset_index(drop = True)

pf = pd.DataFrame({
    'user ID' : user_id,
    'session ID' : session_id,
    'start_time' : start_time,
    'end_time' : end_time,
    'session_request_count' : session_request_count,
    'session_execution_time' : session_execution_time,
    'session_system_execution_time' : session_system_execution_time,
  
})
pf.to_csv(r'C:\Users\KDFX Team\Third.csv')
print('finish')
print(time.time() - time_count)
fig = plt.figure(figsize=(100, 15))

# Divide the figure into a 2x1 grid, and give me the first section
ax1 = fig.add_subplot(221)

# Divide the figure into a 2x1 grid, and give me the second section
ax2 = fig.add_subplot(222)

ax3 = fig.add_subplot(223)


pf.plot(x ='user ID',y = 'session_request_count', x_compat=True, ax=ax1)
pf.plot(x ='user ID',y = ['session_execution_time','session_system_execution_time'] ,ax=ax2)
pf.plot(x ='user ID',y = ['start_time', 'end_time'],ax=ax3)
pf.plot(x ='user ID',y = 'session_request_count',ax=ax1)

fig.savefig('output3.png')

print('finish')
print(time.time() - time_count)
#=============================================================
pd.options.display.max_rows = 4000
df = pd.read_csv('request.log', names=["Date", "Server ID", "Response generation time",
                                       "System response generation time", "Client IP", "Session and user ID","HTTP method",
                                       "URL", "Refferer URL"], sep ='\t', nrows = 10000)
time_count =  time.time()
url_list = df['URL']
url_list1 = []
http_list = []

def find(a):
    if bool(re.search(r'\d|[^\w\//|\/|\:|\.|[а-я]|\s]', a)):
        a = a[0:(a.rfind('/'))]
        return find(a)
    else:
        a = a +'/???/'
        return a
    
url_list_true = []
for i in range (len(url_list)):
    if bool(re.search(r'\d|[^\w\//|\/|\:|\.|[а-я]|\s]', url_list[i])): 
        a = find(url_list[i])
    else:
        a = url_list[i]
    url_list_true.append(a)  
    
url_lest_true1 = []
df['url_list_true']=(df['HTTP method'].astype(str) + '$' + url_list_true)

url_list_count = sorted(df['url_list_true'].value_counts().keys().tolist()) #.loc[(df['Session and user ID'] != '-')]

all_request_count = []
session_request_count = []
all_execution_time = []
session_execution_time = []
session_system_execution_time = []
all_system_execution_time = [] 

for i in range (len(url_list_count)):
    http_list.append(df.loc[df['HTTP method'] == url_list_count[i].split('$')[0]]['HTTP method'].value_counts().keys().tolist())
 
count_and_time('url_list_true',url_list_count)

for i in range (len(url_list_count)):
    url_list_count[i] = url_list_count[i].split('$')[1]
#print()
http_true = [val for sublist in http_list for val in sublist]
ff = pd.DataFrame({
    'HTTP method' : http_true,
    'URL' : url_list_count,
    'all_request_count' : all_request_count,
    'session_request_count' : session_request_count,
    'all_execution_time' :  all_execution_time,
    'session_execution_time' : session_execution_time,
    'all_system_execution_time' : all_system_execution_time,
    'session_system_execution_time' : session_system_execution_time,
    
})

ff = ff[ff['URL']!='-']
ff.to_csv(r'C:\Users\KDFX Team\Fourth.csv')

ff.plot(y=['all_request_count','session_request_count'], x_compat=True)
ax2 = ff.plot(x='URL',secondary_y=[0, 'all_execution_time', 'session_execution_time','all_system_execution_time','session_system_execution_time'], style='--', figsize=(50, 10), ylim =(0)); ax2.grid(); ax2.set_ylim(0)

ax2.set_ylabel('Request count')
ax2.right_ax.set_ylabel('Time(s)')
ax2.get_figure().savefig('output4.png')

print('finish')
print(time.time() - time_count)
ff
#============================================================================
import time
df = pd.read_csv('request.log', names=["Date", "Server ID", "Response generation time",
                                       "System response generation time", "Client IP", "Session and user ID",
                                       "HTTP method", "URL", "Refferer URL"], sep ='\t', nrows = 3000)

time_count =  time.time()
time_column = pd.timedelta_range(0, periods=49, freq='30T')

time_period = [str(time_column[i]) [7:-3] + ' to ' + str(time_column[i+1])[7:-3] for i in range(48)]
df['Date'] = pd.to_datetime(df['Date'].values.astype('datetime64'))
df['Weekday'] = df['Date'].dt.weekday 

result = pd.to_timedelta(df['Date'])
df['delta'] = pd.DataFrame({'result': result})
df['delta'] = df['delta'] - pd.to_timedelta(df['delta'].dt.days, unit='d')
result1 = pd.to_timedelta(df['delta'])//time_column[1]

dfObj = pd.DataFrame(list(zip(result1,df['Weekday'], np.array([1]*len(result1)))), columns = ['Delta' , 'DoW', 'Aggregation'], index=df['Date'].values.astype('datetime64''[D]'))

dfsum = dfObj.groupby(['Delta', 'DoW', dfObj.index]).agg('sum')
dfObj = dfsum.groupby(['DoW','Delta']).agg(['sum', 'mean', 'median'])

period_count = np.zeros(shape=(7,48))

day_of_week = sorted(df['Weekday'].value_counts().keys().tolist())


    


agg_sum = np.zeros(shape=(7,48))
mean = np.zeros(shape=(7,48))
median = np.zeros(shape=(7,48))

for i in range(7):
    for j in range(48):
        try: 
            agg_sum[i][j] = dfObj['Aggregation']['sum'][i][j]
            mean[i][j] = dfObj['Aggregation']['mean'][i][j]
            median[i][j] = dfObj['Aggregation']['median'][i][j]
        except:
            pass


b = time_period

kf_sum =pd.DataFrame(agg_sum, columns = b)
kf_mean = pd.DataFrame(mean, columns = b)
kf_median = pd.DataFrame(median, columns = b)

days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
days_mean = [ 'Monday_mean', 'Tuesday_mean', 'Wednesday_mean', 'Thursday_mean', 'Friday_mean', 'Saturday_mean', 'Sunday_mean']
days_median = [ 'Monday_median', 'Tuesday_median', 'Wednesday_median', 'Thursday_median', 'Friday_median', 'Saturday_median', 'Sunday_median']

kf_sum.insert(0, 'Day of week', days)
kf_mean.insert(0, 'Day of week', days_mean)
kf_median.insert(0, 'Day of week', days_median)


frames = [kf_sum, kf_mean, kf_median]
result = pd.concat(frames) #keys=['sum', 'mean', 'median'])
result.to_csv(r'C:\Users\KDFX Team\Fifth.csv')


ax2 = result.plot(x='Day of week',kind='bar', figsize=(100, 15)); ax2.grid() #(y='Day of week', x_compat=True)
#ax2 = result.plot(x='URL',secondary_y=[0, 'all_execution_time', 'session_execution_time','all_system_execution_time','session_system_execution_time'], style='--', figsize=(50, 10), ylim =(0)); ax2.grid(); ax2.set_ylim(0)

ax2.set_ylabel('Request count')
ax2.get_figure().savefig('output5.png')






print('finish')
print(time.time() - time_count)




result
#plt.scatter(all_request_count, ip_list, label = 'ip')
#=========================================================================
df = pd.read_csv('request.log', names=["Date", "Server ID", "Response generation time",
                                       "System response generation time", "Client IP", "Session and user ID","HTTP method",
                                       "URL", "Refferer URL"], sep ='\t', nrows = 10000) 
def detectSQLi(query):
    #Clear Text SQL injection test, will create false positives. 
    regex=re.compile('drop|delete|truncate|update|insert|select|declare|union|create|concat', re.IGNORECASE)
    if regex.search(query):
        return True

    #look for single quote, = and --
    regex=re.compile('((\%3D)|(=))[^\n]*((\%27)|(\')|(\-\-)|(\%3B)|(;))|\w*((\%27)|(\'))((\%6F)|o|(\%4F))((\%72)|r|(\%52))', re.IGNORECASE)
    if regex.search(query):
        return True
    
    #look for MSExec
    regex=re.compile('exec(\s|\+)+(s|x)p\w+', re.IGNORECASE)
    if regex.search(query):
        return True

    # hex equivalent for single quote, zero or more alphanumeric or underscore characters
    regex=re.compile('/\w*((\%27)|(\'))((\%6F)|o|(\%4F))((\%72)|r|(\%52))/ix', re.IGNORECASE)
    if regex.search(query):
        return True
    
    return False

c = []
for b in range(len(df["URL"])): 
    #print(b)
    c.append(detectSQLi(df["URL"][b]))
    
df['Sql inject'] = c 
sq = df[df['Sql inject'] == True]   
sq.to_csv(r'C:\Users\KDFX Team\sql.csv')
